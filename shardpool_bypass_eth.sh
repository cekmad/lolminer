#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu1-zil.shardpool.io:3333
ZILWALLET=zil1lvfkxjut7nx7x2ql9ud4lty04pxgw8jxcdle2q

ETHPOOL=us1.ethermine.org:4444 
ETHWALLET=377e756be8f047652a97b5cbcd2f86a7fd0f4cae

WORKER=lolMinerWorker

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

./lolMiner --algo ETHASH --pool $POOL --user $ETHWALLET.$WORKER --pass $ZILWALLET@$ETHPOOL $@ --enablezilcache --dualmode zil --dualstratum ETHWALLET@ETHPOOL
